import { CordialclientelePage } from './app.po';

describe('cordialclientele App', () => {
  let page: CordialclientelePage;

  beforeEach(() => {
    page = new CordialclientelePage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
